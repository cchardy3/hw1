#### My name is Courtney Hardy

This is my submission for homework1.
I would like to include one link: [Youtube](https://www.youtube.com/watch?v=_palSgKvFKk&ab_channel=OfficialVGM)

I would also like to include some facts in a table:

|   Item        |          Info            |
|---------------|--------------------------|
| home town     | <Jackson, Mississippi>   |
| favorite song | <Paparazzi by Lady Gaga> |
| <anything else> | <I am excited to learn about Firmware> |

